﻿using UnityEngine;
using System.Collections;

public class scCellularController : MonoBehaviour {
	
	public scGrid levelGrid;

	// Use this for initialization
	void Start () {
		levelGrid = new scGrid(100,100,0,0);
		
		//fill the grid with random noise to begine with
		//48% chance of being a wall
		for (int i = 0; i < levelGrid.getWidth(); i++){
			for (int j = 0; j < levelGrid.getHeight(); j++){
				int num = Random.Range(0,100);
				
				if (num <=48){
					levelGrid.setCell(i,j,1);
				}
			}
		}
		
		for (int k = 0; k < 3; k++){
			generation();
		}
		
		addWallsToEdge();
		
		convertTo3D();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	private void convertTo3D(){
		for (int i = 0; i < levelGrid.getWidth(); i++){
			for (int j = 0; j < levelGrid.getHeight(); j++){
				int num = Random.Range(0,2);
				
				if (levelGrid.getCell(i,j) == 0){
					GameObject aFloor =(GameObject) GameObject.Instantiate(Resources.Load("TileSets/Ground"));
					aFloor.transform.position = new Vector3(i,0,j);
				}
				
				if (levelGrid.getCell(i,j) == 1){
					GameObject aWall =(GameObject) GameObject.Instantiate(Resources.Load("Wall"));
					aWall.transform.position = new Vector3(i, aWall.renderer.bounds.size.y /2,j);
				}
				
			}
		}
		
		GameObject[] allWalls = GameObject.FindGameObjectsWithTag("BaseWall");
		
		GameObject hashCollider = GameObject.FindGameObjectWithTag("CheckCollider");
		hashCollider.GetComponent<scCheckCollider>().setWorldGrid(convertGrid());
		
		foreach(GameObject aWall in allWalls){
			aWall.GetComponent<scWall>().autoTile();	
		}
	}
	
	private void generation(){
		
		for (int i = 0; i < levelGrid.getWidth(); i++){
			for (int j = 0; j < levelGrid.getHeight(); j++){
				calculateScore(i,j);
			}
		}
	}
	
	private void calculateScore(int _x, int _y){
		int score = 0;
		
		if (levelGrid.getCell(_x-1,_y) == 1){
			score++;	
		}
		
		if (levelGrid.getCell(_x+1,_y) == 1){
			score++;	
		}
		
		if (levelGrid.getCell(_x,_y+1) == 1){
			score++;	
		}
		
		if (levelGrid.getCell(_x,_y-1) == 1){
			score++;	
		}
		
		if (levelGrid.getCell(_x-1,_y+1) == 1){
			score++;	
		}
		
		if (levelGrid.getCell(_x+1,_y+1) == 1){
			score++;	
		}
		
		if (levelGrid.getCell(_x-1,_y-1) == 1){
			score++;	
		}
		
		if (levelGrid.getCell(_x+1,_y-1) == 1){
			score++;	
		}
		
		if (levelGrid.getCell(_x,_y) == 1){
			if (score >=4){
				levelGrid.setCell(_x,_y, 1);
			}else{
				levelGrid.setCell(_x,_y, 0);
			}
		}else{
			if (score >=5){
				levelGrid.setCell(_x,_y, 1);
			}else{
				levelGrid.setCell(_x,_y, 0);
			}
				
		}
	}
	
	private void addWallsToEdge(){
		
		for (int i = 0; i < levelGrid.getWidth(); i++){
			for (int j = 0; j < levelGrid.getHeight(); j++){
				if (i == 0){
					levelGrid.setCell(i,j, 1);
				}
				
				if (i == levelGrid.getWidth()-1){
					levelGrid.setCell(i,j, 1);
				}
				
				if (j == 0){
					levelGrid.setCell(i,j, 1);
				}
				
				if (j == levelGrid.getHeight()-1){
					levelGrid.setCell(i,j, 1);
				}
			}
		}
	}
	
	//invert the grid because scCheckCollider works with different values for walls and empty space
	private scGrid convertGrid(){
		scGrid temp = new scGrid(levelGrid.getWidth(), levelGrid.getHeight(),0,0);
		for (int i = 0; i < levelGrid.getWidth(); i++){
			for (int j = 0; j < levelGrid.getHeight(); j++){
				if (levelGrid.getCell(i,j) == 0){
					temp.setCell(i, j, 1);	
				}else{
					temp.setCell(i, j, 0);
				}
				
			}
		}
		
		return temp;
	}
		
		
}
